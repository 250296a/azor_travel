$(document).ready(function () {






  // locale
  $('.locale-js').on('click', function () {
    $(this).next().toggleClass('--open');
  });
  $('.lang-js').on('click', function () {
    var lang = $(this).attr('data-id');
    var flag = $(this).find('img').attr('src');
    $(this).parent().parent().find('.locale-js').attr('data-val', lang);
    $(this).parent().parent().find('.locale-js img').attr('src', flag);
    console.log(flag);
  });
  jQuery(function ($) {
    $(document).mouseup(function (e) {
      var div = $(".locale-js");
      if (!div.is(e.target) &&
        div.has(e.target).length === 0) {
        $('.header__locale-dropdown').removeClass('--open');
      }
    });
  });
  // locale

  // tabs
 
  (function ($) {
    $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');

    $('.tab ul.tabs li a').click(function (g) {
      var tab = $(this).closest('.tab'),
        index = $(this).closest('li').index();

      tab.find('ul.tabs > li').removeClass('current');
      $(this).closest('li').addClass('current');

      tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').hide();
      tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').show();

      g.preventDefault();
    });
  })(jQuery);



  // tabs




  // datapicker


  $(function () {

    $.datepicker.regional['ru'] = {
      closeText: 'Закрыть',
      prevText: '',
      nextText: '',
      currentText: 'Сегодня',
      monthNames: ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня',
        'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'
      ],
      monthNamesShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
        'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
      ],
      dayNames: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
      dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
      dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
      weekHeader: 'Нед',
      dateFormat: 'dd.mm.yy',
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);


    var dateFormat = "dd.mm.yy",
      from = $("#there-date")
      .datepicker({
        numberOfMonths: 2,
        showButtonPanel: true,
        minDate: new Date,
        changeMonth: true,
        changeYear: true,
        showAnim: 'drop',
        setDate: 'today'
      })
      .on("change", function () {
        to.datepicker("option", "minDate", getDate(this));
        $("#back-date").prop("disabled", false);
      }),
      to = $("#back-date").datepicker({
        numberOfMonths: 2,
        showButtonPanel: true,
        minDate: new Date,
        changeMonth: true,
        changeYear: true,
        showAnim: 'drop'
      })
      .on("change", function () {
        from.datepicker("option", "maxDate", getDate(this));
      });

    function getDate(element) {
      var date;
      try {
        date = $.datepicker.parseDate(dateFormat, element.value);
      } catch (error) {
        date = null;
      }

      return date;
    }
  });



  // datapicker end


  // booking class



  $('.unfixed .count').each(function (index) {
    if ($(this).attr('data-count') <= 0) {
      $(this).prev().addClass('disabled');
    }
  });
  $('.fixed .count').each(function (index) {
    if ($(this).attr('data-count') <= 1) {
      $(this).prev().addClass('disabled');
    }
  });

  $('.fixed .minus').on('click', function () {
    var $input = $(this).parent().find('.count');
    var count = parseInt($input.attr('data-count')) - 1;
    count = count < 1 ? 1 : count;
    $input.attr('data-count', count);
    $input.html(count);
    $input.change();
    if (count <= 1) {
      $(this).addClass('disabled');
    }

    var adult = +$('#adult-count').attr('data-count');
    var children = +$('#children-count').attr('data-count');
    var babies = +$('#babies-count').attr('data-count');
    var passenger = (adult + children + babies);
    $('.booking-count-js').html(passenger);
    $('#total').val(passenger);
    $('#adult').val(adult);
    $('#children').val(children);
    $('#babies').val(babies);

    return false;
  });
  $('.unfixed .minus').on('click', function () {
    var $input = $(this).parent().find('.count');
    var count = parseInt($input.attr('data-count')) - 1;
    count = count < 0 ? 0 : count;
    $input.attr('data-count', count);
    $input.html(count);
    $input.change();
    if (count <= 0) {
      $(this).addClass('disabled');
    }

    var adult = +$('#adult-count').attr('data-count');
    var children = +$('#children-count').attr('data-count');
    var babies = +$('#babies-count').attr('data-count');
    var passenger = (adult + children + babies);
    $('.booking-count-js').html(passenger);
    $('#total').val(passenger);
    $('#adult').val(adult);
    $('#children').val(children);
    $('#babies').val(babies);

    return false;
  });
  $('.plus').on('click', function () {
    var $input = $(this).parent().find('.count');
    var count = parseInt($input.attr('data-count')) + 1;
    $input.attr('data-count', count);
    $input.html(count);
    $input.change();
    if (count >= 1) {
      $(this).prev().prev().removeClass('disabled');
    }

    var adult = +$('#adult-count').attr('data-count');
    var children = +$('#children-count').attr('data-count');
    var babies = +$('#babies-count').attr('data-count');
    var passenger = (adult + children + babies);
    $('.booking-count-js').html(passenger);
    $('#total').val(passenger);
    $('#adult').val(adult);
    $('#children').val(children);
    $('#babies').val(babies);

    return false;
  });

  $('.booking__option-class').on('click', function () {
    var bookingClass = $.trim($(this).text());
    $('#booking-class').val(bookingClass);
    $('.booking-class').html(bookingClass);
    $(this).addClass('active');
    $('.booking__option-class').not(this).removeClass('active');
  });

  // booking class end



  // select
  $('.booking__select').on('click', function () {
    $('.booking__select').find('.select__dropdown').addClass('--open');
  });
  $('.select').on('click', function () {
    $(this).not('.booking__select').find('.select__dropdown').toggleClass('--open');
  });
  $('.select__option').on('click', function () {
    $(this).parent().parent().find('input').attr('data-id', $(this).attr('data-id'));
    $(this).parent().parent().find('input').attr('value', $(this).text());
    var val = $(this).parent().parent().find('input').val();
    $(this).parent().parent().find('.select__selected span').text(val);
    $(this).addClass('active');
    $('.select__option').not(this).removeClass('active');
  });

  jQuery(function ($) {
    $(document).mouseup(function (e) {
      var div = $(".select");
      if (!div.is(e.target) &&
        div.has(e.target).length === 0) {
        $('.select__dropdown').removeClass('--open');
      }
    });
  });
  // select end

  // place
  setTimeout(function () {
    $('.input__icon--takeoff').addClass('takeoff');
    $('.input__icon--landing').addClass('landing');
  }, 2000);

  $('.booking__couple--place .form-group input').each(function (index) {
    if (!$(this).val()) {
      $('.input__icon--place').fadeIn();
      $('.input__short-name').fadeOut();
    } else {
      $('.input__icon--place').fadeOut();
      $('.input__short-name').fadeIn();
    }
  });
  $('.booking__couple--place .form-group input').change(function () {
    if (!$(this).val()) {
      $(this).parent().find('.input__icon--place').fadeIn();
      $(this).parent().find('.input__short-name').fadeOut();
    } else {
      $(this).parent().find('.input__icon--place').fadeOut();
      $(this).parent().find('.input__short-name').fadeIn();
    }
  });
  // place end

  $('.booking__reverce').on('click', function () {
    var a = $('#city').val();
    var b = $('#city-dest').val();

    var c = $('#city').val(b);
    var d = $('#city-dest').val(a);


  });

  // products
  var swiper = new Swiper(".products-slider", {
    spaceBetween: 30,
    slidesPerView: 6,
    loop: true,
    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    }
  });
  // products end

  // subscription
  $('.subscription').on('click', function () {
    $('.subscription__hidden').slideDown();
  });
  // subscription end



  // check
  $('.sidebar__section').each(function (index) {
    $('.all-check').click(function () {
      $(this).parent().parent().parent().find('input:checkbox').prop('checked', this.checked);
    });
  });

  // check end


  // sidebar section
  $('.sidebar__section--dropdown .sidebar__section-title').on('click', function () {
    $(this).parent().toggleClass('--open');
    $(this).parent().find('.sidebar__section-content').slideToggle();
  });
  // sidebar section end



  // range input
  // $(function () {
  //   $("#duration-transfers").slider({
  //     range: true,
  //     min: 0,
  //     max: 60,
  //     values: [75, 300],
  //     slide: function (event, ui) {
  //       $("#duration-transfers-amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
  //     }
  //   });
  //   $("#duration-transfers-amount").val("$" + $("#duration-transfers").slider("values", 0) +
  //     " - $" + $("#duration-transfers").slider("values", 1));
  // });


  $(function () {
    $(".airfare-prices").slider({
      range: true,
      min: 0,
      max: 99999,
      values: [0, 99999],
      slide: function (event, ui) {
        $(".airfare-prices-amount").val("от " + ui.values[0] + " - до " + ui.values[1]);
      }
    });
    $(".airfare-prices-amount").val("от " + $(".airfare-prices").slider("values", 0) +
      " - до " + $(".airfare-prices").slider("values", 1));
  });
  // range input end


});